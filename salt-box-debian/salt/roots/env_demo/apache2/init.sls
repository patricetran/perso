include:
  - env_demo.apache2.vhost.vhost_demo

# Install Apache
apache2:
  pkg.installed:
    - name: apache2
 #   - file: apache2
# Start the Apache service
  service.running:
#    - enable: True
    - reload: True
    - watch:
      - file: /etc/apache2/sites-available/httpd-vhosts.conf
    - require:
      - pkg: apache2
# Copy the configuration template to /etc/apache2/apache2.conf and render grains values.
  file.managed:
    - name: /etc/apache2/apache2.conf
    - source: salt://env_demo/apache2/files/apache2.conf
    - require:
      - pkg: apache2

/var/www/test:        
  file.directory:
    - user: root
    - group: root
    - mode: 755
    - makedirs: True
        
# Create index.html file for vhost_demo
/var/www/test/index.html:
  file.managed:
    - source: salt://env_demo/apache2/files/index.html
    - user: root
    - group: root
    - mode: 644
