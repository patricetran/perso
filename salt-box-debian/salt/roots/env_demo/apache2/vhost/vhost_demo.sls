/etc/apache2/sites-available/httpd-vhosts.conf:
  file.managed:
    - source: salt://env_demo/apache2/files/vhost_demo.conf
    - template: jinja

Activate_vhost:
  cmd.run:
    - name: a2ensite httpd-vhosts.conf
#    - require: /etc/apache2/sites-available/httpd-vhosts.conf
