base:
  pkg.installed:
    - pkgs:
        - python-pip
        - python-virtualenv

/home/virtualenv:
  virtualenv.managed:
    - requirements: salt://env_demo/virtualenv/files/requirements.txt
    - unless: test -f /home/virtualenv
